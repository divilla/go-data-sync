--
-- PostgreSQL database dump
--

-- Dumped from database version 14.4
-- Dumped by pg_dump version 14.4

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: truncate_all(); Type: PROCEDURE; Schema: public; Owner: user
--

CREATE PROCEDURE public.truncate_all()
    LANGUAGE plpgsql
    AS $$
begin
truncate table ads;
truncate table ads_replication;
end; $$;


ALTER PROCEDURE public.truncate_all() OWNER TO "user";

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: ads; Type: TABLE; Schema: public; Owner: user
--

CREATE TABLE public.ads (
    id integer NOT NULL,
    region_id smallint NOT NULL,
    category_id smallint NOT NULL,
    subregion_id integer DEFAULT 0 NOT NULL,
    district_id_old smallint,
    district_id smallint,
    city_id integer,
    accurate_location smallint DEFAULT 0 NOT NULL,
    user_id integer NOT NULL,
    created_at timestamp without time zone NOT NULL,
    created_at_first timestamp without time zone,
    created_at_pushup timestamp without time zone,
    valid_to timestamp without time zone NOT NULL,
    status text NOT NULL,
    reason_id smallint,
    remove_reason_details text,
    title text NOT NULL,
    description text NOT NULL,
    phone text,
    params text,
    contactform smallint DEFAULT 0 NOT NULL,
    ip integer DEFAULT 0 NOT NULL,
    map_address text,
    offer_seek text DEFAULT 'offer'::text NOT NULL,
    external_partner_code text,
    external_id text,
    partner_offer_url text,
    private_business text DEFAULT 'private'::text NOT NULL,
    map_zoom smallint DEFAULT 0 NOT NULL,
    map_lat numeric(10,8),
    map_lon numeric(11,8),
    map_radius integer DEFAULT 0 NOT NULL,
    skype text,
    gg text,
    person text NOT NULL,
    visible_in_profile smallint DEFAULT 1 NOT NULL,
    ad_homepage_to timestamp without time zone,
    source text,
    traffic_source text
);


ALTER TABLE public.ads OWNER TO "user";

--
-- Name: ads_replication; Type: TABLE; Schema: public; Owner: user
--

CREATE TABLE public.ads_replication (
    id integer NOT NULL,
    start_emit timestamp without time zone,
    end_emit timestamp without time zone,
    start_consume timestamp without time zone,
    end_consume timestamp without time zone,
    total_rows integer
);


ALTER TABLE public.ads_replication OWNER TO "user";

--
-- Name: ads_replication_view; Type: VIEW; Schema: public; Owner: user
--

CREATE VIEW public.ads_replication_view AS
 SELECT ads_replication.id,
    ads_replication.start_emit,
    ads_replication.end_emit,
    ads_replication.start_consume,
    ads_replication.end_consume,
    ads_replication.total_rows,
    EXTRACT(epoch FROM (ads_replication.end_emit - ads_replication.start_emit)) AS emit_duration,
    EXTRACT(epoch FROM (ads_replication.end_consume - ads_replication.start_consume)) AS consume_duration,
    EXTRACT(epoch FROM (ads_replication.end_consume - ads_replication.end_emit)) AS latency
   FROM public.ads_replication;


ALTER TABLE public.ads_replication_view OWNER TO "user";


--
-- Name: ads ads_pkey; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.ads
    ADD CONSTRAINT ads_pkey PRIMARY KEY (id);


--
-- Name: ads_replication ads_replication_pk; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.ads_replication
    ADD CONSTRAINT ads_replication_pk PRIMARY KEY (id);


--
-- Name: ad_homepage_to; Type: INDEX; Schema: public; Owner: user
--

CREATE INDEX ad_homepage_to ON public.ads USING btree (ad_homepage_to);


--
-- Name: category_id; Type: INDEX; Schema: public; Owner: user
--

CREATE INDEX category_id ON public.ads USING btree (category_id, created_at);


--
-- Name: created; Type: INDEX; Schema: public; Owner: user
--

CREATE INDEX created ON public.ads USING btree (created_at);


--
-- Name: created_at_first; Type: INDEX; Schema: public; Owner: user
--

CREATE INDEX created_at_first ON public.ads USING btree (created_at_first);


--
-- Name: external; Type: INDEX; Schema: public; Owner: user
--

CREATE INDEX external ON public.ads USING btree (external_id);


--
-- Name: external_partner_code; Type: INDEX; Schema: public; Owner: user
--

CREATE INDEX external_partner_code ON public.ads USING btree (external_partner_code);


--
-- Name: person; Type: INDEX; Schema: public; Owner: user
--

CREATE INDEX person ON public.ads USING btree (person);


--
-- Name: title_ap; Type: INDEX; Schema: public; Owner: user
--

CREATE INDEX title_ap ON public.ads USING btree (title);


--
-- Name: user_id; Type: INDEX; Schema: public; Owner: user
--

CREATE INDEX user_id ON public.ads USING btree (user_id, visible_in_profile, status, created_at);


--
-- Name: valid_to; Type: INDEX; Schema: public; Owner: user
--

CREATE INDEX valid_to ON public.ads USING btree (status, valid_to);


--
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: user
--

REVOKE ALL ON SCHEMA public FROM postgres;
REVOKE ALL ON SCHEMA public FROM PUBLIC;
GRANT ALL ON SCHEMA public TO "user";
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

