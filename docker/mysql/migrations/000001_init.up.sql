-- MySQL dump 10.13  Distrib 5.7.38, for Linux (x86_64)
--
-- Host: localhost    Database: olx
-- ------------------------------------------------------
-- Server version	5.7.38-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
SET @MYSQLDUMP_TEMP_LOG_BIN = @@SESSION.SQL_LOG_BIN;
SET @@SESSION.SQL_LOG_BIN= 0;

--
-- Table structure for table `ads`
--

DROP TABLE IF EXISTS `ads`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ads` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `region_id` smallint(5) unsigned NOT NULL,
  `category_id` smallint(5) unsigned NOT NULL,
  `subregion_id` int(10) unsigned NOT NULL DEFAULT '0',
  `district_id_old` smallint(5) unsigned DEFAULT NULL,
  `district_id` smallint(5) unsigned DEFAULT NULL,
  `city_id` int(11) unsigned DEFAULT NULL,
  `accurate_location` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `user_id` int(10) unsigned NOT NULL,
  `created_at` datetime NOT NULL,
  `created_at_first` datetime DEFAULT NULL,
  `created_at_pushup` datetime DEFAULT NULL,
  `valid_to` datetime NOT NULL,
  `status` enum('unconfirmed','active','removed_by_user','moderated','blocked','disabled','outdated','new','removed_by_moderator','limited','unpaid') NOT NULL,
  `reason_id` tinyint(5) unsigned DEFAULT NULL,
  `remove_reason_details` varchar(255) DEFAULT NULL,
  `title` char(255) NOT NULL,
  `description` text NOT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `params` text,
  `contactform` tinyint(1) NOT NULL DEFAULT '0',
  `ip` int(10) unsigned NOT NULL DEFAULT '0',
  `map_address` varchar(255) DEFAULT NULL,
  `offer_seek` enum('offer','seek') NOT NULL DEFAULT 'offer',
  `external_partner_code` varchar(50) DEFAULT NULL,
  `external_id` varchar(255) DEFAULT NULL,
  `partner_offer_url` varchar(255) DEFAULT NULL,
  `private_business` enum('private','business') NOT NULL DEFAULT 'private',
  `map_zoom` tinyint(1) NOT NULL DEFAULT '0',
  `map_lat` decimal(10,8) DEFAULT NULL,
  `map_lon` decimal(11,8) DEFAULT NULL,
  `map_radius` int(10) unsigned NOT NULL DEFAULT '0',
  `skype` varchar(255) DEFAULT NULL,
  `gg` varchar(50) DEFAULT NULL,
  `person` varchar(100) NOT NULL,
  `visible_in_profile` tinyint(1) NOT NULL DEFAULT '1',
  `ad_homepage_to` datetime DEFAULT NULL,
  `source` enum('none','i','m','android','apple','i2','wp') DEFAULT NULL,
  `traffic_source` enum('crm','direct','newsletter','organic','paid','paid_google_branded','paid_google_gdn','paid_google_nonbranded','referral','social') DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ad_homepage_to` (`ad_homepage_to`),
  KEY `category_id` (`category_id`,`created_at`),
  KEY `created` (`created_at`),
  KEY `created_at_first` (`created_at_first`),
  KEY `external` (`external_id`),
  KEY `external_partner_code` (`external_partner_code`),
  KEY `person` (`person`(3)),
  KEY `title_ap` (`title`(2)),
  KEY `user_id` (`user_id`,`visible_in_profile`,`status`,`created_at`),
  KEY `valid_to` (`status`,`valid_to`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ads_outbox`
--

DROP TABLE IF EXISTS `ads_outbox`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ads_outbox` (
  `id` bigint(20) unsigned NOT NULL,
  `version` int(10) unsigned NOT NULL,
  `operation` enum('insert','update','delete') NOT NULL,
  `region_id` smallint(5) unsigned DEFAULT NULL,
  `category_id` smallint(5) unsigned DEFAULT NULL,
  `subregion_id` int(10) unsigned DEFAULT NULL,
  `district_id_old` smallint(5) unsigned DEFAULT NULL,
  `district_id` smallint(5) unsigned DEFAULT NULL,
  `city_id` int(10) unsigned DEFAULT NULL,
  `accurate_location` tinyint(3) unsigned DEFAULT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_at_first` datetime DEFAULT NULL,
  `created_at_pushup` datetime DEFAULT NULL,
  `valid_to` datetime DEFAULT NULL,
  `status` char(20) DEFAULT NULL,
  `reason_id` tinyint(3) unsigned DEFAULT NULL,
  `remove_reason_details` varchar(255) DEFAULT NULL,
  `title` char(255) DEFAULT NULL,
  `description` text,
  `phone` varchar(50) DEFAULT NULL,
  `params` text,
  `contactform` tinyint(4) DEFAULT NULL,
  `ip` int(10) unsigned DEFAULT NULL,
  `map_address` varchar(255) DEFAULT NULL,
  `offer_seek` char(5) DEFAULT NULL,
  `external_partner_code` varchar(50) DEFAULT NULL,
  `external_id` varchar(255) DEFAULT NULL,
  `partner_offer_url` varchar(255) DEFAULT NULL,
  `private_business` char(8) DEFAULT NULL,
  `map_zoom` tinyint(4) DEFAULT NULL,
  `map_lat` decimal(10,8) DEFAULT NULL,
  `map_lon` decimal(11,8) DEFAULT NULL,
  `map_radius` int(10) unsigned DEFAULT NULL,
  `skype` varchar(255) DEFAULT NULL,
  `gg` varchar(50) DEFAULT NULL,
  `person` varchar(100) DEFAULT NULL,
  `visible_in_profile` tinyint(4) DEFAULT NULL,
  `ad_homepage_to` datetime DEFAULT NULL,
  `source` char(7) DEFAULT NULL,
  `traffic_source` char(22) DEFAULT NULL,
  `outbox_version` int(10) unsigned NOT NULL DEFAULT '0',
  `outbox_created_at` bigint(20) unsigned NOT NULL DEFAULT '0',
  `outbox_modified_at` bigint(20) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`,`version`),
  KEY `ads_outbox_modified_at` (`outbox_modified_at`),
  KEY `ads_outbox_operation_index` (`operation`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ads_replication`
--

DROP TABLE IF EXISTS `ads_replication`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ads_replication` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `rows` int(11) DEFAULT NULL,
  `start` datetime(6) DEFAULT NULL,
  `end` datetime(6) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ads_trigger`
--

DROP TABLE IF EXISTS `ads_trigger`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ads_trigger` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `region_id` smallint(5) unsigned DEFAULT NULL,
  `category_id` smallint(5) unsigned DEFAULT NULL,
  `subregion_id` int(10) unsigned DEFAULT NULL,
  `district_id_old` smallint(5) unsigned DEFAULT NULL,
  `district_id` smallint(5) unsigned DEFAULT NULL,
  `city_id` int(10) unsigned DEFAULT NULL,
  `accurate_location` tinyint(3) unsigned DEFAULT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_at_first` datetime DEFAULT NULL,
  `created_at_pushup` datetime DEFAULT NULL,
  `valid_to` datetime DEFAULT NULL,
  `status` char(20) DEFAULT NULL,
  `reason_id` tinyint(3) unsigned DEFAULT NULL,
  `remove_reason_details` varchar(255) DEFAULT NULL,
  `title` char(255) DEFAULT NULL,
  `description` text,
  `phone` varchar(50) DEFAULT NULL,
  `params` text,
  `contactform` tinyint(4) DEFAULT NULL,
  `ip` int(10) unsigned DEFAULT NULL,
  `map_address` varchar(255) DEFAULT NULL,
  `offer_seek` char(5) DEFAULT NULL,
  `external_partner_code` varchar(50) DEFAULT NULL,
  `external_id` varchar(255) DEFAULT NULL,
  `partner_offer_url` varchar(255) DEFAULT NULL,
  `private_business` char(8) DEFAULT NULL,
  `map_zoom` tinyint(4) DEFAULT NULL,
  `map_lat` decimal(10,8) DEFAULT NULL,
  `map_lon` decimal(11,8) DEFAULT NULL,
  `map_radius` int(10) unsigned DEFAULT NULL,
  `skype` varchar(255) DEFAULT NULL,
  `gg` varchar(50) DEFAULT NULL,
  `person` varchar(100) DEFAULT NULL,
  `visible_in_profile` tinyint(4) DEFAULT NULL,
  `ad_homepage_to` datetime DEFAULT NULL,
  `source` char(7) DEFAULT NULL,
  `traffic_source` char(22) DEFAULT NULL,
  `outbox_version` int(10) unsigned NOT NULL DEFAULT '0',
  `outbox_created_at` bigint(20) unsigned NOT NULL DEFAULT '0',
  `outbox_modified_at` bigint(20) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `ad_homepage_to` (`ad_homepage_to`),
  KEY `category_id` (`category_id`,`created_at`),
  KEY `created` (`created_at`),
  KEY `created_at_first` (`created_at_first`),
  KEY `external` (`external_id`),
  KEY `external_partner_code` (`external_partner_code`),
  KEY `person` (`person`(3)),
  KEY `title_ap` (`title`(2)),
  KEY `user_id` (`user_id`,`visible_in_profile`,`status`,`created_at`),
  KEY `valid_to` (`status`,`valid_to`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `dates_and_times`
--

DROP TABLE IF EXISTS `dates_and_times`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dates_and_times` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `c_date` date DEFAULT NULL,
  `c_time` time DEFAULT NULL,
  `c_datetime` datetime DEFAULT NULL,
  `c_timestamp` timestamp(6) NULL DEFAULT NULL,
  `c_year` year(4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `numerics`
--

DROP TABLE IF EXISTS `numerics`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `numerics` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `c_bit` bit(1) DEFAULT NULL,
  `c_tinyint` tinyint(4) DEFAULT NULL,
  `c_bool` tinyint(1) DEFAULT NULL,
  `c_boolean` tinyint(1) DEFAULT NULL,
  `c_smallint` smallint(6) DEFAULT NULL,
  `c_mediumint` mediumint(9) DEFAULT NULL,
  `c_int` int(11) DEFAULT NULL,
  `c_integer` int(11) DEFAULT NULL,
  `c_bigint` bigint(20) DEFAULT NULL,
  `c_decimal` decimal(12,2) DEFAULT NULL,
  `c_dec` decimal(12,0) DEFAULT NULL,
  `c_float` float unsigned DEFAULT NULL,
  `c_float_precision` float DEFAULT NULL,
  `c_double` double DEFAULT NULL,
  `c_double_precision` double DEFAULT NULL,
  `c_real` double DEFAULT NULL,
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `strings`
--

DROP TABLE IF EXISTS `strings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `strings` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `c_char` char(10) DEFAULT NULL,
  `c_varchar` varchar(100) DEFAULT NULL,
  `c_binary` binary(10) DEFAULT NULL,
  `c_varbinary` varbinary(100) DEFAULT NULL,
  `c_tinytext` tinytext,
  `c_mediumtext` mediumtext,
  `c_longtext` longtext,
  `c_tinyblob` tinyblob,
  `c_mediumblob` mediumblob,
  `c_longblob` longblob,
  `c_enum` enum('enum1','enum2','enum3') DEFAULT NULL,
  `c_set` set('set1','set2','set3') DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
