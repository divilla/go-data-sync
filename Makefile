MODULE = $(shell go list -m)
VERSION ?= $(shell git describe --tags --always --dirty --match=v* 2> /dev/null || echo "1.0.0")
PACKAGES := $(shell go list ./... | grep -v /vendor/)
#LDFLAGS := -ldflags "-X main.Version=${VERSION}"

TIMESTAMP=$(shell date +'%Y-%m-%d_%H:%M:%S')
MYSQL_CONTAINER = ds-mysql-olx-pl
MYSQL_DB_NAME = olx
POSTGRES_CONTAINER = ds-postgres-olx-ads
POSTGRES_DB_NAME = olx_ads

#CONFIG_FILE ?= ./config/local.yml
#APP_DSN ?= $(shell sed -n 's/^dsn:[[:space:]]*"\(.*\)"/\1/p' $(CONFIG_FILE))
#MIGRATE := docker run -v $(shell pwd)/migrations:/migrations --network host migrate/migrate:v4.10.0 -path=/migrations/ -database "$(APP_DSN)"

#PID_FILE := './.pid'
#FSWATCH_FILE := './fswatch.cfg'

.PHONY: default
default: help

# generate help info from comments: thanks to https://marmelab.com/blog/2016/02/29/auto-documented-makefile.html
.PHONY: help
help: ## help information about make commands
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

.PHONY: test
test: ## run unit tests
	@echo "mode: count" > coverage-all.out
	@$(foreach pkg,$(PACKAGES), \
		go test -p=1 -cover -covermode=count -coverprofile=coverage.out ${pkg}; \
		tail -n +2 coverage.out >> coverage-all.out;)

.PHONY: test-cover
test-cover: test ## run unit tests and show test coverage information
	go tool cover -html=coverage-all.out

.PHONY: run
run: ## run the API server
	go run cmd/producer/main.go &
	go run cmd/consumer/main.go &

.PHONY: run-restart
run-restart: ## restart the API server
	@pkill -P `cat $(PID_FILE)` || true
	@printf '%*s\n' "80" '' | tr ' ' -
	@echo "Source file changed. Restarting server..."
	@go run ${LDFLAGS} cmd/server/main.go & echo $$! > $(PID_FILE)
	@printf '%*s\n' "80" '' | tr ' ' -

run-live: ## run the API server with live reload support (requires fswatch)
	@go run ${LDFLAGS} cmd/server/main.go & echo $$! > $(PID_FILE)
	@fswatch -x -o --event Created --event Updated --event Renamed -r internal pkg cmd config | xargs -n1 -I {} make run-restart

.PHONY: build
build:  ## build the API server binary
	CGO_ENABLED=1 go build ${LDFLAGS} -a -o cmd/build/producer cmd/producer/main.go

.PHONY: build-docker
build-docker: ## build the API server as a docker image
	docker build -f cmd/producer/Dockerfile -t server .

.PHONY: clean
clean: ## remove temporary files
	rm -rf server coverage.out coverage-all.out

.PHONY: version
version: ## display the version of the API server
	@echo $(VERSION)

.PHONY: ssh-mysql
ssh-mysql: ## ssh to MySQL
	docker exec -it ${MYSQL_CONTAINER} /bin/bash

.PHONY: ssh-postgresql
ssh-postgresql: ## ssh to PostgreSQL
	docker exec -it ${POSTGRES_CONTAINER} /bin/bash

.PHONY: ssh-kafka-1
ssh-kafka-1: ## ssh to Kafka Broker 1
	docker exec -it ds-kafka-1 /bin/bash

.PHONY: backup-mysql
backup-mysql: ## backup MySQL database
	mkdir -p docker/mysql/dump
	docker exec -it ${MYSQL_CONTAINER} \
		/opt/bitnami/mysql/bin/mysqldump -u root ${MYSQL_DB_NAME} > docker/mysql/backup.sql
	cp docker/mysql/backup.sql docker/mysql/dump/${MYSQL_DB_NAME}_${TIMESTAMP}.sql

.PHONY: restore-mysql
restore-mysql: ## restore MySQL database
	cat docker/mysql/backup.sql | \
	docker exec -i ${MYSQL_CONTAINER} \
		/opt/bitnami/mysql/bin/mysql -u root ${MYSQL_DB_NAME}

.PHONY: migrate-mysql
migrate-mysql: backup-mysql ## create MySQL migrations
	mkdir -p docker/mysql/migrations
	migrate create -dir docker/mysql/migrations/ -ext sql -seq init
	cp --copy-contents docker/mysql/backup.sql docker/mysql/migrations/000001_init.up.sql

.PHONY: backup-postgres
backup-postgres: ## backup postgres database
	mkdir -p docker/postgres/dump
	docker exec -it ${POSTGRES_CONTAINER} \
		/opt/bitnami/postgresql/bin/pg_dump --dbname=postgresql://user:pass@127.0.0.1:5432/${POSTGRES_DB_NAME} > docker/postgres/backup.sql
	cp docker/postgres/backup.sql docker/postgres/dump/${POSTGRES_DB_NAME}_${TIMESTAMP}.sql

.PHONY: restore-postgres
restore-postgres: ## restore postgres database
	docker exec -i ${POSTGRES_CONTAINER} \
		/opt/bitnami/postgresql/bin/psql --dbname=postgresql://user:pass@127.0.0.1:5432/${POSTGRES_DB_NAME} < docker/postgres/backup.sql

.PHONY: migrate-postgres
migrate-postgres: backup-postgres ## create PostgreSQL migrations
	mkdir -p docker/postgres/migrations
	migrate create -dir docker/postgres/migrations/ -ext sql -seq init
	cp --copy-contents docker/postgres/backup.sql docker/postgres/migrations/000001_init.up.sql



LOGFILE=$(shell date +'%Y-%m-%d-%H-%M-%S')
.PHONY: date
date:
	$(LOGFILE)

.PHONY: testdata
testdata: ## populate the database with test data
	make migrate-reset
	@echo "Populating test data..."
	@docker exec -it postgres psql "$(APP_DSN)" -f /testdata/testdata.sql

.PHONY: lint
lint: ## run golint on all Go package
	@golint $(PACKAGES)

.PHONY: fmt
fmt: ## run "go fmt" on all Go packages
	@go fmt $(PACKAGES)
