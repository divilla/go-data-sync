package xkafka

import (
	"github.com/confluentinc/confluent-kafka-go/kafka"
	"go.uber.org/zap"
)

type Producer struct {
	*kafka.Producer
	logger *zap.Logger
}

func NewProducer(bootstrapServers string, logger *zap.Logger) *Producer {
	p, err := kafka.NewProducer(&kafka.ConfigMap{
		"bootstrap.servers": bootstrapServers,
	})
	if err != nil {
		panic(err)
	}

	// Delivery report handler for produced messages
	go func() {
		for {
			for e := range p.Events() {
				switch ev := e.(type) {
				case *kafka.Message:
					if ev.TopicPartition.Error != nil {
						logger.Warn("delivery failed",
							zap.Int32("TopicPartition", ev.TopicPartition.Partition),
							zap.String("Key", string(ev.Key)),
							zap.Error(ev.TopicPartition.Error),
						)
					}
				}
			}
		}
	}()

	return &Producer{
		p,
		logger,
	}
}

func (p *Producer) Publish(topic string, key []byte, value []byte) error {
	return p.Produce(&kafka.Message{
		TopicPartition: kafka.TopicPartition{
			Topic:     &topic,
			Partition: kafka.PartitionAny,
		},
		Key:   key,
		Value: value,
	}, nil)
}

func (p *Producer) Close() {
	p.Producer.Flush(10000)
	p.Producer.Close()
}
