# Go Data Sync

## Getting started

```shell
# Make sure you don't have local MySQL or PostgreSQL running - then it will not work
docker-compose up

# Wait about minute or more for MySQL to fully start
```



## Create Kafka topic

```shell
# Open new screen in terminal, stay in the same directory
make ssh-kafka-1
cd /opt/bitnami/kafka/bin
kafka-topics.sh --create --topic "leg_pl_ads" --bootstrap-server "ds-kafka-1:9092,ds-kafka-2:9092,ds-kafka-3:9092" --partitions 3 --replication-factor 3

# Check whether topic is created - should return "leg_pl_ads"
kafka-topics.sh --list --bootstrap-server "ds-kafka-1:9092,ds-kafka-2:9092,ds-kafka-3:9092"

# Exit
exit
```


## Start producer & consumer

```shell
make run

# Wait for something like [info] binlogsyncer.go:808 rotate to (bin.000001, 4)
# Don't close terminal
```


## Start MySQL heavy loader

```shell
# Start new terminal & cd to go-mysql-heavy-loader
cd ../go-mysql-heavy-loader

# Check config file
nano config/config.yaml

# Adjust "concurrent_operations" to your total number of threads / 2
# Adjust "operation_distribution" to 1,0,0 - let it do only inserts to seed data
make run

# Switch to second terminal - where you started consumer/producer
# You should see {"msg":"ads_replication", ... {"action": "insert"}
# Wait for {"msg":"ads_replication", ... {"action": "update"}

# Start your favorite DB IDE
# Connect to MySQL server: "localhost:3306", user: "user", password: "pass", db: "olx"
# Connect to PostgreSQL server: "localhost:5432", user: "user", password: "pass", db: "olx_ads"

# In PostgreSQL open table "ads_replication" it will show statistics

# If you got slow results first time, just repeat inserts again on MySQL Heavy Loader terminal
make run

# Open config/config.yaml and set "operation_distribution" to 2,7,1
make run

# In PostgreSQL open table "ads_replication" it will show statistics
```



## Authors and acknowledgment
ljudevit.ocic@olx.pl

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
