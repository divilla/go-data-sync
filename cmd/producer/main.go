package main

import (
	"fmt"
	"github.com/go-mysql-org/go-mysql/canal"
	"github.com/go-mysql-org/go-mysql/client"
	"gitlab.com/divilla/go-data-sync/internal/binlog"
	"gitlab.com/divilla/go-data-sync/internal/config"
	"gitlab.com/divilla/go-data-sync/pkg/xkafka"
	"go.uber.org/zap"
)

func main() {
	logger, err := zap.NewProduction()
	if err != nil {
		panic(err)
	}
	defer func(logger *zap.Logger) {
		if err = logger.Sync(); err != nil {
			fmt.Println(err)
		}
	}(logger)

	cfg := config.New("config/config.yaml")
	canCfg := canal.NewDefaultConfig()
	canCfg.Addr = cfg.MysqlAddr
	canCfg.User = cfg.MysqlUser
	canCfg.Password = cfg.MysqlPassword
	canCfg.Dump.ExecutionPath = cfg.MysqlDumpExecutionPath
	canCfg.UseDecimal = cfg.MysqlUseDecimal
	canCfg.IncludeTableRegex = []string{`olx\.ads`}

	if cfg.MysqlResetMaster {
		conn, err := client.Connect(cfg.MysqlAddr, cfg.MysqlUser, cfg.MysqlPassword, "")
		if err != nil {
			panic(err)
		}
		if _, err = conn.Execute("PURGE BINARY LOGS BEFORE NOW();"); err != nil {
			panic(err)
		}
		if _, err = conn.Execute("RESET MASTER;"); err != nil {
			panic(err)
		}
		if err = conn.Close(); err != nil {
			panic(err)
		}
	}

	c, err := canal.NewCanal(canCfg)
	if err != nil {
		logger.Error("failed canal.NewCanal", zap.Error(err))
	}

	kp := xkafka.NewProducer(cfg.KafkaBootstrapServers, logger)
	defer kp.Close()
	c.SetEventHandler(binlog.NewEventHandler(kp, logger))

	if err = c.Run(); err != nil {
		logger.Error("failed to start Canal", zap.Error(err))
	}
}
