#!/bin/bash -e

#exec > >(tee -a /var/log/app.log|logger -t /bin/app -s 2>/dev/console) 2>&1

CONFIG_FILE=./config/config.yaml

if [[ -z ${APP_DSN} ]]; then
  export APP_DSN=`sed -n 's/^ms_dsn:[[:space:]]*"\(.*\)"/\1/p' ${CONFIG_FILE}`
fi

echo "[`date`] Running DB migrations..."
migrate -database "${APP_DSN}" -path ./docker/mysql/migrations up

echo "[`date`] Starting app..."
./bin/app >> /var/log/app.log 2>&1
