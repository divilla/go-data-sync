# first stage
FROM golang:1.17.2-stretch as build_base
WORKDIR /src
COPY go.mod .
COPY go.sum .
RUN go mod download

# second stage
FROM build_base AS builder
WORKDIR /src
COPY . .
RUN CGO_ENABLED=1 GOOS=linux GOARCH=amd64 go build -tags netgo -o /bin/app -ldflags "-w -s" ./cmd/consumer/main.go

# final stage
FROM debian:10.3-slim
RUN apt-get update && apt-get install -y --no-install-recommends apt-utils ca-certificates

# install migrate which will be used by entrypoint.sh to perform DB migration
ARG MIGRATE_VERSION=4.15.2
ADD https://github.com/golang-migrate/migrate/releases/download/v${MIGRATE_VERSION}/migrate.linux-amd64.tar.gz /tmp
RUN tar -xzf /tmp/migrate.linux-amd64.tar.gz -C /bin

COPY --from=builder /bin/app /bin/app
COPY --from=builder /src/docker/postgres/migrations ./docker/postgres/migrations/
COPY --from=builder /src/cmd/consumer/entrypoint.sh .
COPY --from=builder /src/config/*.yaml ./config/

ENTRYPOINT ["./entrypoint.sh"]
