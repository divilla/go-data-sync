package main

import (
	"context"
	"fmt"
	"github.com/confluentinc/confluent-kafka-go/kafka"
	"gitlab.com/divilla/go-data-sync/internal/config"
	"gitlab.com/divilla/go-data-sync/internal/procmsg"
	"gitlab.com/divilla/go-data-sync/pkg/xpg"
	"go.uber.org/zap"
	"os"
	"os/signal"
	"sync"
	"syscall"
	"time"
)

func main() {
	logger, err := zap.NewProduction()
	if err != nil {
		panic(err)
	}
	defer func(logger *zap.Logger) {
		if err = logger.Sync(); err != nil {
			fmt.Println(err)
		}
	}(logger)

	cfg := config.New("config/config.yaml")

	pgp := xpg.NewPool(cfg.DSN)
	conn, err := pgp.Acquire(context.Background())
	if err != nil {
		panic(err)
	}
	defer conn.Release()

	c, err := kafka.NewConsumer(&kafka.ConfigMap{
		"bootstrap.servers": cfg.KafkaBootstrapServers,
		"group.id":          cfg.KafkaGroupId,
		//"auto.offset.reset": cfg.KafkaAutoOffsetReset,
	})
	if err != nil {
		panic(err)
	}
	defer func(c *kafka.Consumer) {
		if err = c.Close(); err != nil {
			fmt.Println(err)
		}
	}(c)

	if err = c.SubscribeTopics(cfg.KafkaSubscribeTopics, nil); err != nil {
		panic(err)
	}
	defer func(c *kafka.Consumer) {
		if err = c.Close(); err != nil {
			logger.Error("failed to close Kafka subscription")
		}
	}(c)

	sigCh := make(chan os.Signal)
	signal.Notify(sigCh, os.Interrupt, syscall.SIGTERM)

	msgChs := make([]chan []byte, cfg.ConsumerProcesses)
	doneCh := make(chan int)
	cancelCh := make(chan struct{})
	doneWg := &sync.WaitGroup{}
	timeout := time.Duration(cfg.CommandTimeout) * time.Second

	go func() {
		<-sigCh
		logger.Info("stopping consumer")
		doneWg.Wait()
		os.Exit(0)
	}()

	for procId := 0; procId < cfg.ConsumerProcesses; procId++ {
		msgChs[procId] = make(chan []byte)
		go procmsg.Process(logger, pgp, doneWg, msgChs[procId], doneCh, cancelCh, timeout, procId)
	}

	logger.Info("starting consumer")

Loop:
	for {
		select {
		case <-cancelCh:
			break Loop
		case procId := <-doneCh:
			if msg, err := c.ReadMessage(-1); err != nil {
				logger.Warn("Kafka read message error", zap.Error(err))
			} else {
				doneWg.Add(1)
				msgChs[procId] <- msg.Value
			}
		}
	}
}
