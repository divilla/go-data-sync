package binlog

import (
	"fmt"
	"github.com/go-mysql-org/go-mysql/canal"
	"github.com/tidwall/sjson"
	"gitlab.com/divilla/go-data-sync/pkg/xkafka"
	"go.uber.org/zap"
	"strings"
)

type EventHandler struct {
	canal.DummyEventHandler
	producer *xkafka.Producer
	logger   *zap.Logger
}

func NewEventHandler(producer *xkafka.Producer, logger *zap.Logger) *EventHandler {
	return &EventHandler{
		DummyEventHandler: canal.DummyEventHandler{},
		producer:          producer,
		logger:            logger,
	}
}

func (h *EventHandler) OnRow(e *canal.RowsEvent) error {
	//h.logger.Info(fmt.Sprintf("%s %s %v\n", e.Table.Name, e.Action, e.Rows))

	var err error
	var colName string
	var keys []string
	var jsonBytes []byte

	if jsonBytes, err = h.setJson(jsonBytes, "domain", "pl"); err != nil {
		return err
	}
	if jsonBytes, err = h.setJson(jsonBytes, "table", e.Table.Name); err != nil {
		return err
	}
	if jsonBytes, err = h.setJson(jsonBytes, "action", e.Action); err != nil {
		return err
	}

	if e.Action == canal.InsertAction {
		for _, key := range e.Table.PKColumns {
			oldValue := h.fixValue(e.Table.Columns[key].RawType, e.Rows[0][key])
			keys = append(keys, parseKey(oldValue))
		}

		for key := range e.Rows[0] {
			colName = e.Table.Columns[key].Name
			newValue := h.fixValue(e.Table.Columns[key].RawType, e.Rows[0][key])

			if jsonBytes, err = h.setJson(jsonBytes, dataJsonColName(colName), newValue); err != nil {
				return err
			}
		}
	}

	if e.Action == canal.UpdateAction {
		for _, key := range e.Table.PKColumns {
			colName = e.Table.Columns[key].Name
			oldValue := h.fixValue(e.Table.Columns[key].RawType, e.Rows[0][key])

			keys = append(keys, parseKey(oldValue))
			if jsonBytes, err = h.setJson(jsonBytes, pkJsonColName(colName), oldValue); err != nil {
				return err
			}
		}

		for key := range e.Rows[0] {
			oldValue := h.fixValue(e.Table.Columns[key].RawType, e.Rows[0][key])
			newValue := h.fixValue(e.Table.Columns[key].RawType, e.Rows[1][key])

			if oldValue != newValue {
				colName = e.Table.Columns[key].Name
				if jsonBytes, err = h.setJson(jsonBytes, dataJsonColName(colName), newValue); err != nil {
					return err
				}
			}
		}
	}

	if e.Action == canal.DeleteAction {
		for _, key := range e.Table.PKColumns {
			colName = e.Table.Columns[key].Name
			oldValue := h.fixValue(e.Table.Columns[key].RawType, e.Rows[0][key])

			keys = append(keys, parseKey(oldValue))
			if jsonBytes, err = h.setJson(jsonBytes, pkJsonColName(colName), oldValue); err != nil {
				return err
			}
		}
	}

	return h.producer.Publish("leg_pl_ads", parseKeys(keys), jsonBytes)
}

func (h *EventHandler) String() string {
	return "EventHandler"
}

func (h EventHandler) fixValue(rawType string, val interface{}) interface{} {
	if val == nil {
		return nil
	}

	switch rawType {
	case "tinytext":
		return string(val.([]byte))
	case "text":
		return string(val.([]byte))
	case "mediumtext":
		return string(val.([]byte))
	case "longtext":
		return string(val.([]byte))
	default:
		return val
	}
}

func (h EventHandler) setJson(json []byte, path string, value interface{}) ([]byte, error) {
	if jsonBytes, err := sjson.SetBytes(json, path, value); err != nil {
		return nil, fmt.Errorf("sjson.Set failed %w", err)
	} else {
		return jsonBytes, nil
	}
}

func parseKeys(input []string) []byte {
	return []byte(strings.Join(input, "-"))
}

func parseKey(input interface{}) string {
	return fmt.Sprintf("%v", input)
}

func pkJsonColName(input string) string {
	return fmt.Sprintf("pk.%s", input)
}

func dataJsonColName(input string) string {
	return fmt.Sprintf("data.%s", input)
}
