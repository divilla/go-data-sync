package config

import (
	"gopkg.in/yaml.v3"
	"io/ioutil"
)

type Config struct {
	DSN   string `yaml:"dsn"`
	MSDSN string `yaml:"ms_dsn"`
	PGDSN string `yaml:"pg_dsn"`

	MysqlAddr              string   `yaml:"mysql_addr"`
	MysqlUser              string   `yaml:"mysql_user"`
	MysqlPassword          string   `yaml:"mysql_password"`
	MysqlDatabase          string   `yaml:"mysql_database"`
	MysqlIncludeTableRegex []string `yaml:"mysql_include_table_regex"`
	MysqlDumpExecutionPath string   `yaml:"mysql_dump_execution_path"`
	MysqlUseDecimal        bool     `yaml:"mysql_use_decimal"`
	MysqlResetMaster       bool     `yaml:"mysql_reset_master"`

	KafkaBootstrapServers string   `yaml:"kafka_bootstrap_servers"`
	KafkaGroupId          string   `yaml:"kafka_group_id"`
	KafkaAutoOffsetReset  string   `yaml:"kafka_auto_offset_reset"`
	KafkaSubscribeTopics  []string `yaml:"kafka_subscribe_topics"`

	ConsumerProcesses int `yaml:"consumer_processes"`
	CommandTimeout    int `yaml:"command_timeout"`
}

func New(path string) *Config {
	file, err := ioutil.ReadFile(path)
	if err != nil {
		panic(err)
	}

	var config Config
	if err = yaml.Unmarshal(file, &config); err != nil {
		panic(err)
	}

	return &config
}
