package procmsg

import (
	"context"
	"fmt"
	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/tidwall/gjson"
	"gitlab.com/divilla/go-data-sync/pkg/xpg"
	"go.uber.org/zap"
	"strings"
	"sync"
	"time"
)

func Process(
	logger *zap.Logger,
	pgp *xpg.Pool,
	doneWg *sync.WaitGroup,
	msgCh <-chan []byte,
	doneCh chan<- int,
	cancelCh <-chan struct{},
	timeout time.Duration,
	procId int,
) {

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	conn, err := pgp.Acquire(ctx)
	if err != nil {
		logger.Warn("failed to acquire PostgresSQL connection", zap.Error(err))
		return
	}
	defer conn.Release()

	doneCh <- procId

Loop:
	for {
		select {
		case <-cancelCh:
			break Loop
		case msg := <-msgCh:
			doneWg.Add(1)
			process(ctx, logger, conn, doneWg, doneCh, timeout, procId, msg)
		}
	}

}

func process(
	parentCtx context.Context,
	logger *zap.Logger,
	conn *pgxpool.Conn,
	doneWg *sync.WaitGroup,
	doneCh chan<- int,
	timeout time.Duration,
	procId int,
	msg []byte,
) {

	ctx, cancel := context.WithTimeout(parentCtx, timeout)
	defer func() {
		cancel()
		doneWg.Done()
		doneCh <- procId
	}()

	var err error
	action := gjson.GetBytes(msg, "action").String()
	table := gjson.GetBytes(msg, "table").String()

	if table == "ads_replication" {
		logger.Info("ads_replication", zap.ByteString("message.Value", msg))
	}
	if table == "ads_replication" && action == "insert" {
		id := gjson.GetBytes(msg, "data.id").Int()
		start := gjson.GetBytes(msg, "data.start").String()
		if _, err = conn.Exec(ctx, "INSERT INTO ads_replication (id, start_emit, start_consume) VALUES ($1, $2, now())", id, start); err != nil {
			logger.Error("failed to insert into ads_replication", zap.Error(err))
		}
		return
	}
	if table == "ads_replication" && action == "update" {
		id := gjson.GetBytes(msg, "pk.id").Int()
		end := gjson.GetBytes(msg, "data.end").String()
		rows := gjson.GetBytes(msg, "data.rows").Int()
		if _, err = conn.Exec(ctx, "UPDATE ads_replication SET end_emit = $1, end_consume = now(), total_rows = $2 WHERE id = $3;", end, rows, id); err != nil {
			logger.Error("failed to insert into ads_replication", zap.Error(err))
		}
		return
	}

	var statement string
	var args []interface{}
	switch action {
	case "insert":
		statement, args = Insert(msg)
	case "update":
		statement, args = Update(msg)
	case "delete":
		statement, args = Delete(msg)
	default:
		logger.Error("invalid action in Kafka message", zap.String("action", action))
	}
	_, err = conn.Exec(ctx, statement, args...)
	if err != nil {
		logger.Warn("failed to execute commend", zap.Error(err))
	}
}

func Insert(msg []byte) (string, []interface{}) {
	var colNames, colNumbers []string
	var colValues []interface{}
	table := gjson.GetBytes(msg, "table").String()
	i := 1
	gjson.GetBytes(msg, "data").ForEach(func(key, value gjson.Result) bool {
		colNames = append(colNames, key.Raw)
		colNumbers = append(colNumbers, fmt.Sprintf("$%v", i))
		if value.Type == gjson.String {
			colValues = append(colValues, value.Value())
		} else {
			colValues = append(colValues, value.Raw)
		}
		i++
		return true
	})
	statement := strings.Join([]string{
		"INSERT INTO ",
		table,
		" (",
		strings.Join(colNames, ", "),
		") VALUES (",
		strings.Join(colNumbers, ", "),
		");",
	}, "")

	return statement, colValues
}

func Update(msg []byte) (string, []interface{}) {
	var colSets, pkCond []string
	var colValues []interface{}
	table := gjson.GetBytes(msg, "table").String()
	i := 1

	gjson.GetBytes(msg, "data").ForEach(func(key, value gjson.Result) bool {
		colSets = append(colSets, fmt.Sprintf("%s=$%v", key.Raw, i))
		if value.Type == gjson.String {
			colValues = append(colValues, value.Value())
		} else {
			colValues = append(colValues, value.Raw)
		}

		i++
		return true
	})

	gjson.GetBytes(msg, "pk").ForEach(func(key, value gjson.Result) bool {
		pkCond = append(pkCond, fmt.Sprintf("%s=$%v", key.Raw, i))
		if value.Type == gjson.String {
			colValues = append(colValues, value.Value())
		} else {
			colValues = append(colValues, value.Raw)
		}

		i++
		return true
	})

	statement := strings.Join([]string{
		"UPDATE ",
		table,
		" SET ",
		strings.Join(colSets, ", "),
		" WHERE ",
		strings.Join(pkCond, " AND "),
		";",
	}, "")

	return statement, colValues
}

func Delete(msg []byte) (string, []interface{}) {
	var pkCond []string
	var colValues []interface{}
	table := gjson.GetBytes(msg, "table").String()
	i := 1

	gjson.GetBytes(msg, "pk").ForEach(func(key, value gjson.Result) bool {
		pkCond = append(pkCond, fmt.Sprintf("%s=$%v", key.Raw, i))
		if value.Type == gjson.String {
			colValues = append(colValues, value.Value())
		} else {
			colValues = append(colValues, value.Raw)
		}
		i++
		return true
	})

	statement := strings.Join([]string{
		"DELETE FROM ",
		table,
		" WHERE ",
		strings.Join(pkCond, " AND "),
		";",
	}, "")

	return statement, colValues
}
